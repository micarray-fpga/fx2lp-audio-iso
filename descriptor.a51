; FX2LP USB audio class firmware
;
; Copyright (C) 2009 Ubixum, Inc.
; Copyright (C) 2023 Jan Šedivý
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 2 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License along
; with this program; if not, write to the Free Software Foundation, Inc.,
; 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
;
; version 1.7
; change however you want but leave
; the descriptor pointers so the setupdat.c file works right
;-------------------------------------------------------------------------------

; CONFIGURATION PARAMETERS

; Sampling frequency [Hz]
F_SAMP = 48000 ; 48 kHz default

; Total number of sound channels
CHANNELS = 8

; Maximum packet size [bytes]
; 1024 bytes for high speed ISO enpoint with one packet per microframe
; This is enough for 32 channels at 48 kHz: 48e3 * 32 * 4 * 125e-6 = 768
AUDIO_PACKET_SIZE = 1024

; vendor and product ID
VID = 0xCAFE
PID = 0x1337

;-------------------------------------------------------------------------------

.module DEV_DSCR

; CONSTANTS

; descriptor types
; same as setupdat.h
DSCR_DEVICE_TYPE=1
DSCR_CONFIG_TYPE=2
DSCR_STRING_TYPE=3
DSCR_INTERFACE_TYPE=4
DSCR_ENDPOINT_TYPE=5
DSCR_DEVQUAL_TYPE=6

; for the repeating interfaces
DSCR_INTERFACE_LEN=9
DSCR_ENDPOINT_LEN=7

; endpoint types
ENDPOINT_TYPE_CONTROL=0
ENDPOINT_TYPE_ISO=1
ENDPOINT_TYPE_BULK=2
ENDPOINT_TYPE_INT=3

.globl	_dev_dscr, _dev_qual_dscr, _highspd_dscr, _fullspd_dscr, _dev_strings, _dev_strings_end

; These need to be in code memory. If they aren't you'll have to manually
; copy them somewhere in code memory otherwise SUDPTRH:L won't work right
.area	DSCR_AREA	(CODE)

;-------------------------------------------------------------------------------
; Device Descriptor

_dev_dscr:
	.db	dev_dscr_end-_dev_dscr	; length
	.db	DSCR_DEVICE_TYPE		; descrripto type (device)
	.dw	0x0002					; usb 2.0
	.db 0x00					; class - interface specific
	.db 0x00					; subclass - none
	.db 0x00					; protocol - interface specific
	.db	64						; max packet size (ep0)
	.db	(VID % 256)				; vendor id LSB
	.db	(VID / 256)				; vendor id MSB
	.db	(PID % 256)				; product id LSB
	.db	(PID / 256)				; product id MSB
	.dw	0x0100					; device version
	.db	1						; manufacturer string idx
	.db	2						; product string idx
	.db	0						; serial string idx
	.db	1						; n configurations
dev_dscr_end:

_dev_qual_dscr:
	.db	dev_qualdscr_end-_dev_qual_dscr
	.db	DSCR_DEVQUAL_TYPE
	.dw	0x0002					; usb 2.0
	.db	0xff
	.db	0xff
	.db	0xff
	.db	64						; max packet
	.db	1						; n configs
	.db	0						; extra reserved byte
dev_qualdscr_end:

;-------------------------------------------------------------------------------
; USB high speed configuration descriptor
; descriptor structure taken from AN295 - USB AUDIO CLASS TUTORIAL
; https://www.silabs.com/documents/public/application-notes/AN295.pdf

; header
_highspd_dscr:
	.db	highspd_dscr_end-_highspd_dscr	; descriptor header length
	.db	DSCR_CONFIG_TYPE
	; can't use .dw because byte order is different
	.db	(highspd_dscr_realend-_highspd_dscr) % 256 ; total length of config LSB
	.db	(highspd_dscr_realend-_highspd_dscr) / 256 ; total length of config MSB
	; two interfaces: audio control + audio streaming
	.db	2				; number of used interfaces
	.db	1				; config number
	.db	0				; config string
	.db	0x80			; attributes = bus powered, no wakeup
	.db	0x32			; max power = 100 mA
highspd_dscr_end:

; Audio Control Interface Descriptor
	.db	DSCR_INTERFACE_LEN
	.db	DSCR_INTERFACE_TYPE
	.db	0				; interface index
	.db	0				; alt setting index
	.db	0				; n endpoints
	.db	0x01			; interface class (audio)
	.db	0x01			; interface subclass (audio control)
	.db	0x00			; interface protocol (none)
	.db	0				; string index

; Interface Header Audio Class Descriptor (class specific)
audio_class:
	.db	DSCR_INTERFACE_LEN	; header length
	.db 0x24	; bDescriptorType (CS_INTERFACE)
	.db 0x01	; bDescriptorSubtype (HEADER)
	.dw 0x0001	; Audio class 1.0
	.db	(audio_class_end - audio_class) % 256 ; total length LSB
	.db	(audio_class_end - audio_class) / 256 ; total length MSB
	.db 0x01	; one audio stream
	.db 0x01	; interface 1 is audio input stream

; Input Terminal Audio Class Descriptor
	.db 0x0C	; length (12)
	.db 0x24	; bDescriptorType (CS_INTERFACE)
	.db 0x02	; bDescriptorSubtype (INPUT_TERMINAL)
	.db 0x01	; bTerminalID (1)
	.dw 0x0502	; wTerminalType (Microphone array)
	.db 0x00	; bAssocTerminal (none)
	; This is not the actual channel number, but we dont use channel controls
	; If it were parametrized, Feature Unit descriptor would have variable
	; length, which would complicate things
	.db 0x02	; bNrChannels (2)
	.dw 0x0300	; wChannelConfig - stereo (left, right)
	.db 0x00	; iChannelNames (none)
	.db 0x00	; iTerminal (none)

; Feature Unit Audio Class Descriptor
	.db 0x0D	; bLength (13)
	.db 0x24	; bDescriptorType (CS_INTERFACE)
	.db 0x06	; bDescriptorSubtype (FEATURE_UNIT)
	.db 0x02	; bUnitID (2)
	.db 0x01	; bSourceID (input terminal 1)

	.db 0x02	; bControlSize (2 bytes)
	.dw 0x0100	; Master controls
	.dw 0x0000	; Channel 0 controls
	.dw 0x0000	; Channel 1 controls
	.db 0x00	; iFeature (none)

; Output Terminal Audio Class Descriptor
	.db 9		; length
	.db 0x24	; bDescriptorType (CS_INTERFACE)
	.db 0x03	; bDescriptorSubtype (OUTPUT_TERMINAL)
	.db 0x03	; bTerminalID (3)
	.dw 0x0101	; wTerminalType (USB streaming)
	.db 0x00	; bAssocTerminal (none)
	.db 0x02	; bSourceID (feature unit 2)
	.db 0x00	; iTerminal (none)

audio_class_end:

; Audio Interface Descriptor
; USB Specification requires that interfaces with an isochronous endpoint
; must also define an alternative interface without that isochronous endpoint.

; alternate setting 0: inactive interface (default)
	.db	DSCR_INTERFACE_LEN
	.db	DSCR_INTERFACE_TYPE
	.db	1				; interface index
	.db	0				; alt setting index
	.db	0				; n endpoints
	.db	0x01			; interface class (audio)
	.db	0x02			; interface subclass (audio streaming)
	.db	0x00			; interface protocol (none)
	.db	0x00			; string index (none)

; Alternate Audio Interface Descriptor
; alternate setting 1: interface that uses an isochronous endpoint
	.db	DSCR_INTERFACE_LEN
	.db	DSCR_INTERFACE_TYPE
	.db	1				; interface index
	.db	1				; alt setting index
	.db	1				; n endpoints
	.db	0x01			; interface class (audio)
	.db	0x02			; interface subclass (audio streaming)
	.db	0x00			; interface protocol (none)
	.db	3				; string index

; Audio Stream Audio Class Descriptor
	.db 0x07	; bLength (7)
	.db 0x24	; bDescriptorType (CS_INTERFACE)
	.db 0x01	; bDescriptorSubtype (AS_GENERAL)
	.db 0x03	; bTerminalLink (terminal 3)
	.db 0x00	; bDelay (none)
	.dw 0x0100	; wFormatTag (PCM format)

; [USB high speed] data polling interval = (2 ^ (INTERVAL-1)) x 125 us
INTERVAL = 1 ; polling interval 125 us

; Format Type Audio Descriptor
	.db 0x0B	; bLength (11)
	.db 0x24	; bDescriptorType (CS_INTERFACE)
	.db 0x02	; bDescriptorSubtype (FORMAT_TYPE)
	.db 0x01	; bFormatType (TYPE_I) - no compression
	.db CHANNELS	; bNrChannels
	.db 0x04	; bSubFrameSize - sample size 4 bytes
	.db 24		; bBitResolution (24)
	.db 0x01	; bSamFreqType (single sampling frequency)
	.db (F_SAMP % 256) ; byte 0 (LSB)
	.db ((F_SAMP / 256) & 0xFF) ; byte 1
	.db ((F_SAMP / 65536) & 0xFF) ; byte 2 (MSB)

; Isochronous Endpoint Descriptor
	.db 0x09	; length (9)
	.db	DSCR_ENDPOINT_TYPE
	.db	0x86	; endpoint 6 IN
	.db	0x05	; bmAttributes (asynchronous)
	.db (AUDIO_PACKET_SIZE & 0xFF) ; max packet size LSB
	.db ((AUDIO_PACKET_SIZE / 256) & 0xFF) ; max packet size MSB
	.db	INTERVAL	; data polling interval
	.db 0x00	; bRefresh (0)
	.db 0x00	; bSynchAddress (no synchronization)

; Isochronous Endpoint Audio Class Descriptor
	.db 0x07	; bLength (7)
	.db 0x25	; bDescriptorType (CS_ENDPOINT)
	.db 0x01	; bDescriptorSubtype (EP_GENERAL)
	.db 0x00	; bmAttributes (none)
	.db 0x02	; bLockDelayUnits (PCM samples)
	.dw 0x0000	; wLockDelay (0)

highspd_dscr_realend:

;-------------------------------------------------------------------------------
; USB full speed descriptor
; only bulk transfer
; Unsupported

.even
_fullspd_dscr:
	.db	fullspd_dscr_end-_fullspd_dscr	; dscr len
	.db	DSCR_CONFIG_TYPE
	; can't use .dw because byte order is different
	.db	(fullspd_dscr_realend-_fullspd_dscr) % 256 ; total length of config LSB
	.db	(fullspd_dscr_realend-_fullspd_dscr) / 256 ; total length of config MSB
	.db	1				; n interfaces
	.db	1				; config number
	.db	0				; config string
	.db	0x80			; attrs = bus powered, no wakeup
	.db	0x32			; max power = 100ma
fullspd_dscr_end:

; all the interfaces next
; NOTE the default TRM actually has more alt interfaces
; but you can add them back in if you need them.
; here, we just use the default alt setting 1 from the TRM
	.db	DSCR_INTERFACE_LEN
	.db	DSCR_INTERFACE_TYPE
	.db	0				; index
	.db	0				; alt setting idx
	.db	1				; n endpoints
	.db	0xff			; class
	.db	0xff
	.db	0xff
	.db	3				; string index

; endpoint 6 IN
	.db	DSCR_ENDPOINT_LEN
	.db	DSCR_ENDPOINT_TYPE
	.db	0x86				; ep6 direction = IN and address
	.db	ENDPOINT_TYPE_BULK	; type
	.db	0x40				; max packet LSB
	.db	0x00				; max packet size = 64 bytes
	.db	0x00				; polling interval

fullspd_dscr_realend:

;-------------------------------------------------------------------------------
; Strings
; USB strings are encoded in UTF-16 little-endian

.even
_dev_strings:

; String Descriptor Zero, Specifying Languages Supported by the Device
_string0:
	.db	string0end-_string0 ; length
	.db	DSCR_STRING_TYPE
	.db 0x09, 0x04		; wLANGID: English (United States) (0x0409)
string0end:

; Manufacturer string
; ČVUT
_string1:
	.db string1end-_string1
	.db DSCR_STRING_TYPE
	.db 0x0C
	.db 0x01
	.ascii 'V'
	.db 0
	.ascii 'U'
	.db 0
	.ascii 'T'
	.db 0
string1end:

; Product string
_string2:
	.db string2end-_string2
	.db DSCR_STRING_TYPE
	.ascii 'F'
	.db 0
	.ascii 'P'
	.db 0
	.ascii 'G'
	.db 0
	.ascii 'A'
	.db 0
	.ascii ' '
	.db 0
	.ascii 'M'
	.db 0
	.ascii 'i'
	.db 0
	.ascii 'c'
	.db 0
	.ascii 'A'
	.db 0
	.ascii 'r'
	.db 0
	.ascii 'r'
	.db 0
	.ascii 'a'
	.db 0
	.ascii 'y'
	.db 0
	.ascii 'B'
	.db 0
	.ascii 'o'
	.db 0
	.ascii 'a'
	.db 0
	.ascii 'r'
	.db 0
	.ascii 'd'
	.db 0
string2end:

; Audio Interface alternate setting 1
_string3:
	.db string3end-_string3
	.db DSCR_STRING_TYPE
	.ascii 'A'
	.db 0
	.ascii 'u'
	.db 0
	.ascii 'd'
	.db 0
	.ascii 'i'
	.db 0
	.ascii 'o'
	.db 0
	.ascii ' '
	.db 0
	.ascii 'S'
	.db 0
	.ascii 't'
	.db 0
	.ascii 'r'
	.db 0
	.ascii 'e'
	.db 0
	.ascii 'a'
	.db 0
	.ascii 'm'
	.db 0
string3end:

_dev_strings_end:
	; just in case someone passes an index higher than the end to the firmware
	.dw 0x0000

