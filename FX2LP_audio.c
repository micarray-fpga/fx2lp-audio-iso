/**
 * FX2LP USB audio class firmware
 *
 * Copyright (C) 2009 Ubixum, Inc.
 * Copyright (C) 2023 Jan Šedivý
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

// verze 3.4
// [v3.1] - implementován ischronní přenos namísto bulk
// [v3.2] - autopaket se zapne až po přepnutí rozhraní
//	do alternativní konfigurace 1
// [v3.3] - RENUMERATE_UNCOND se zavolá až po nastavení USB registrů
// [v3.4] - vypnutí automatického odesílání paketu (to zajistí FPGA)
//------------------------------------------------------------------------------

#include <fx2regs.h>
#include <fx2macros.h>
#include <serial.h>
#include <delay.h>
#include <autovector.h>
#include <lights.h>
#include <setupdat.h>
#include <eputils.h>

#define SYNCDELAY SYNCDELAY4

// použít FIFO hodiny 30 MHz?
// __FPGA MicArrayBoard nezvládne 48 MHz__
#define FIFO30M

// flag: received new setupdata
volatile __bit got_sud;

// nastavená konfigurace USB zařízení
volatile int konfigurace = -1;

// alternativní konfigurace rozhraní 1 (audio stream)
volatile BYTE alt_interface = 0;

//------------------------------------------------------------------------------
static void setup_usb(void) {

	// zapnutí specifických funkcí čipu (nejsou zde tolik důležité)
	REVCTL = bmBIT0 | bmBIT1;

	// Vnitřní zdroj hodin 48MHz pro FIFO,
	// FIFO v synchronním (IFCONFIG.3 = 0) režimu slave (IFCFG1:0 = 11)
	IFCONFIG = 0xE3;
	SYNCDELAY;

#ifdef FIFO30M
	SETIF30MHZ();
	SYNCDELAY;
#endif

	// FLAGA = EP2 empty, FLAGB = EP6 full
	PINFLAGSAB = 0xE8;
	SYNCDELAY;

	// FLAGC = EP6 empty
	PINFLAGSCD = 0x0A;
	SYNCDELAY;

	// PA7 má funkci SLCS
	PORTACFG &= ~bmBIT7;
	SYNCDELAY;
	PORTACFG |= bmBIT6;
	SYNCDELAY;

	// only valid endpoint is 6 - disable all others
	EP1INCFG &= ~bmVALID;
	SYNCDELAY;
	EP1OUTCFG &= ~bmVALID;
	SYNCDELAY;

	EP2CFG &= ~bmVALID;
	SYNCDELAY;

	EP4CFG &= ~bmVALID;
	SYNCDELAY;

	EP8CFG &= ~bmVALID;
	SYNCDELAY;

	// EP6 IN isochronous, 2 x 1024 byte buffer
	EP6CFG = 0b11011010;
	SYNCDELAY;

	// nulovací sekvence všech FIFO
	FIFORESET = 0x80;
	SYNCDELAY;
	FIFORESET = 0x02;
	SYNCDELAY;
	FIFORESET = 0x04;
	SYNCDELAY;
	FIFORESET = 0x06;
	SYNCDELAY;
	FIFORESET = 0x08;
	SYNCDELAY;
	FIFORESET = 0x00;
	SYNCDELAY;

	// 8 bitová sběrnice
	EP6FIFOCFG &= ~bmWORDWIDE;
	SYNCDELAY;

	// vypne automatické odesílání paketů
	EP6FIFOCFG &= ~bmAUTOIN;
	SYNCDELAY;

	// bit 7: AADJ - Auto Adjust
	// FX2LP automatically manages the data PID sequencing for high-speed EP
	EP6ISOINPKTS |= 1 << 7;
	SYNCDELAY;

}

//------------------------------------------------------------------------------
void main() {
	USE_USB_INTS();
	ENABLE_SUDAV();
	ENABLE_SOF();
	ENABLE_HISPEED();
	ENABLE_USBRESET();

	// takt procesoru 48 MHz
	SETCPUFREQ(CLK_48M);

	// PA0 jako výstup
	OEA |= 1;
	// zápis 1 na PA0 - LED svítí
	PA0 = 1;

	got_sud = FALSE;

	FIFOPINPOLAR = 0x00; // set all slave FIFO interface pins as active low
	SYNCDELAY;

	setup_usb();

	EA = 1; // global __interrupt enable

	// renumerate
	RENUMERATE_UNCOND();

	while(TRUE) {
		// čekání na příjem řídicího paketu (CONTROL)
		if (got_sud) {
			// volání obslužné funkce knihovny
			handle_setupdata();
			got_sud = FALSE;
		}
	}
}

//------------------------------------------------------------------------------
// copied routines from setupdat.h

// value (low byte) = ep
#define VC_EPSTAT 0xB1


BOOL handle_vendorcommand(BYTE cmd)
{
	__xdata BYTE* pep;
	switch ( cmd ) {
	case VC_EPSTAT:

		pep = ep_addr(SETUPDAT[2]);
		if (pep) {
			EP0BUF[0] = *pep;
			EP0BCH=0;
			EP0BCL=1;
			return TRUE;
		}
	default:
		break;
	}
	return FALSE;
}

// this firmware only supports interface 0
BOOL handle_get_interface(BYTE ifc, BYTE* alt_ifc)
{
	if (ifc == 0) {
		*alt_ifc = alt_interface;
		return TRUE;
	} else {
		return FALSE;
	}
}

BOOL handle_set_interface(BYTE ifc, BYTE alt_ifc)
{

	if (ifc == 1 && alt_ifc == 0) {
		// posílání vzorků není aktivní
		PA0 = 1;
		SYNCDELAY;

		// SEE TRM 2.3.7
		// reset toggle
		RESETTOGGLE(0x86);

		// restore endpoints to default condition
		RESETFIFO(0x06);

		alt_interface = 0;

		return TRUE;

	} else if (ifc == 1 && alt_ifc == 1) {
		// posílání vzorků je zahájeno ze strany PC - signalizování na PA0
		PA0 = 0;
		SYNCDELAY;

		RESETTOGGLE(0x86);
		RESETFIFO(0x06);

		alt_interface = 1;

		return TRUE;
	} else
		return FALSE;
}

BOOL handle_get_descriptor(BYTE desc)
{
	(void) desc;// nepoužitý parametr
	return FALSE;
}

// get/set configuration
BYTE handle_get_configuration()
{
	return 1;
}

BOOL handle_set_configuration(BYTE cfg)
{
	konfigurace = cfg;

	return cfg==1 ? TRUE : FALSE; // we only handle cfg 1
}

void sudav_isr() __interrupt SUDAV_ISR
{
	got_sud=TRUE;
	CLEAR_SUDAV();
}

void sof_isr () __interrupt SOF_ISR __using 1
{
	CLEAR_SOF();
}

void usbreset_isr() __interrupt USBRESET_ISR
{
	handle_hispeed(FALSE);
	CLEAR_USBRESET();
}

void hispeed_isr() __interrupt HISPEED_ISR
{
	handle_hispeed(TRUE);
	CLEAR_HISPEED();
}

