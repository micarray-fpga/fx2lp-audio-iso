#-------------------------------------------------------------------------------
# Makefile to build FX2LP firmware with fx2lib library
#-------------------------------------------------------------------------------

# Location of the fx2lib library
FX2LIBDIR ?= ../fx2lib

# Firmware name
BASENAME = FX2LP_audio

# Firmware sources
SOURCES = FX2LP_audio.c
A51_SOURCES = descriptor.a51

# VID and PID of the compiled firmware (in hexadecimal)
VID := cafe
PID := 1337

# VID and PID of the libfpgalink standard firmware
# This VID and PID is used for identification of the target chip when
#	loading firmware
STD_VID := 1d50
STD_PID := 602b

#-------------------------------------------------------------------------------
# Definitions taken from $(FX2LIBDIR)/lib/fx2.mk

AS8051 ?= sdas8051
DSCR_AREA ?= -Wl"-b DSCR_AREA=0x3e00"
INT2JT ?= -Wl"-b INT2JT=0x3f00"
CC = sdcc
# these are pretty good settings for most firmwares.
# Have to be careful with memory locations for
# firmwares that require more xram etc.
CODE_SIZE ?= --code-size 0x3c00
XRAM_SIZE ?= --xram-size 0x0200
XRAM_LOC ?= --xram-loc 0x3c00
BUILDDIR ?= build

RELS=$(addprefix $(BUILDDIR)/, $(addsuffix .rel, $(notdir $(basename $(SOURCES) $(A51_SOURCES)))))

LINKFLAGS = $(CODE_SIZE) \
	$(XRAM_SIZE) \
	$(XRAM_LOC) \
	$(DSCR_AREA) \
	$(INT2JT)

all: ihx
ihx: $(BUILDDIR)/$(BASENAME).ihx

# Compile the fx2lib library if required
$(FX2LIBDIR)/lib/fx2.lib: $(FX2LIBDIR)/lib/*.c $(FX2LIBDIR)/lib/*.a51
	$(MAKE) -C $(FX2LIBDIR)/lib

# Create build directory
$(BUILDDIR):
	mkdir -p $(BUILDDIR)

# Compile firmware into Intel hex file
$(BUILDDIR)/$(BASENAME).ihx: $(BUILDDIR) $(RELS) $(FX2LIBDIR)/lib/fx2.lib $(DEPS)
	$(CC) -mmcs51 $(SDCCFLAGS) -o $@ $(RELS) fx2.lib $(LINKFLAGS) -L $(FX2LIBDIR)/lib

%.rel: ../%.c
	$(CC) -mmcs51 $(SDCCFLAGS) -c -o $@ -I $(FX2LIBDIR)/include $<

%.rel: ../%.a51
	$(AS8051) -logs $@ $<

#-------------------------------------------------------------------------------

# Download firmware to RAM of the FX2LP microcontroller using fx2cli
download: build/$(BASENAME).ihx
	fx2cli --vidpid ${STD_VID}:${STD_PID} $< ram
	@sleep 2.4 && lsusb | grep $(PID)

# Get path of the USB device node
DEV = $(shell lsusb | grep ${STD_VID}:${STD_PID} | \
	cut -d' ' -f2,4 | tr ": " " /" )

# Download firmware to RAM using the fxload utility
fxload: build/$(BASENAME).ihx
	@if [ -z ${DEV} ]; then echo "Board not found";exit 1; fi
	/sbin/fxload -D /dev/bus/usb/${DEV} -t fx2lp -I $<
	@sleep 2.4 && lsusb | grep $(PID)

clean:
	rm -rf $(BUILDDIR)

.PHONY: all clean download fxload ihx

